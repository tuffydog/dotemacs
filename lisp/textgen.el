;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Text insertion commands and functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'cl)

(defun md5-region (beg end)
  "Calculate MD5 sum of highlighted region."

  (interactive "r")
  (completion--replace beg end
                       (md5 (buffer-substring-no-properties beg end))))

(defun sha1-region (beg end)
  "Calculate MD5 sum of highlighted region."

  (interactive "r")
  (completion--replace beg end
                       (sha1 (buffer-substring-no-properties beg end))))

(defun mk-latex-ref (label)
  "Make LaTex label or hyperref"

  (interactive "Mlabel:")
  (if mark-active
      (let* ((start (region-beginning))
             (end (region-end))
             (original-string (buffer-substring-no-properties start end)))
        (completion--replace
         start end (format "\\hyperref[%s]{%s}" label original-string)))
    (insert (format "\\label{%s}" label))))

(defun mk-c-cast (beg end)
  "wrap a region in a C cast"

  (interactive "r")
  (let ((type (read-string "C Type: ")))
    (completion--replace beg end
                         (format "(%s)(%s)" type
                                 (buffer-substring-no-properties beg end)))))

(defun mk-c-macro (goal-column)
  "append macro continuation characters on lines in region"

  (interactive "P")
  (progn
    (when (> (mark t) (point))
      (exchange-point-and-mark))
    (let ((start (region-beginning))
          (end (region-end)))
      (save-excursion
        (setq end (copy-marker end))
        (goto-char start)
        (while (< (point) end)
          (forward-char (- (point-at-eol) (point)))
          (while (< (current-column) (max (- (if goal-column
                                                 goal-column
                                               69) 2) 0))
            (insert " "))
          (insert " \\")
          (forward-line 1))
        (move-marker end nil))
      (setq deactivate-mark nil)
      (set-mark start))))

(defun value-to-digits (value)
  (if (> value 0)
      (cons (% value 10)
            (value-to-digits (/ value 10)))
    '()))

(defun subscript (value)
  (interactive "p")
  (insert (apply 'concat
                 (mapcar (lambda (digit) (make-string 1 (case digit
                                                          (0 8320)
                                                          (1 8321)
                                                          (2 8322)
                                                          (3 8323)
                                                          (4 8324)
                                                          (5 8325)
                                                          (6 8326)
                                                          (7 8327)
                                                          (8 8328)
                                                          (9 8329))))
                         (nreverse (value-to-digits value))))))

(defun superscript (value)
  (interactive "p")
  (insert (apply 'concat
                 (mapcar (lambda (digit) (make-string 1 (case digit
                                                          (0 8304)
                                                          (1 185)
                                                          (2 178)
                                                          (3 179)
                                                          (4 8308)
                                                          (5 8309)
                                                          (6 8310)
                                                          (7 8311)
                                                          (8 8312)
                                                          (9 8313))))
                         (nreverse (value-to-digits value))))))

(provide 'textgen)
