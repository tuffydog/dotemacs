(require 'cl)

(defun dwim-narrow-to-region ()
  "If region active, narrow to it, otherwise widen"
  (interactive)
  (if mark-active
      (progn (narrow-to-region (region-beginning)
                               (region-end))
             (cua--deactivate))
    (widen)))

(defun start-or-end-macro ()
  "End macro if defining macro, otherwise start new macro"
  (interactive)
  (if defining-kbd-macro
      (kmacro-end-macro nil)
    (kmacro-start-macro nil)))

(defun jl/join-line-backward ()
  "Joins the current line with the previous line"
  (interactive)
  (save-excursion
    (if mark-active
        (jl/join-region)
      (join-line))))

(defun jl/join-line-forward ()
  "Joins the current line with the next line"
  (interactive)
  (save-excursion
    (if mark-active
        (jl/join-region)
      (join-line 1))))

(defun jl/join-region ()
  "Joins all the lines in the active region"
  (goto-char (region-end))
  (while (> (count-lines (region-beginning) (region-end)) 1)
    (join-line)))

(defmacro unicode-move-word (func-name char-check-func char-move-func)
  `(lexical-let ((words '(Lu Ll Lt Lm Lo Mn Mc Me Nd Nl No Pc))
                 (symbols '(Pd Ps Pe Pi Pf Po Sm Sc Sk So))
                 (whitespace '(Zs Zl Zp Cc Cf Cs Co Cn))
                 (table (unicode-property-table-internal 'general-category)))
     (defun ,func-name (&optional arg)
       (interactive "p")
       (dotimes (i (if arg arg 1))
         (let ((initial-char (,char-check-func)))
           (when initial-char
             (let* ((initial-category (get-unicode-property-internal
                                       table
                                       initial-char))
                    (skip-category
                     (cond ((memq initial-category words) words)
                           ((memq initial-category symbols) symbols)
                           ((memq initial-category whitespace) whitespace))))
               (when skip-category
                 (,char-move-func)
                 (while (let ((next-char (,char-check-func)))
                          (and next-char (memq (get-unicode-property-internal
                                                table
                                                next-char) skip-category)))
                   (,char-move-func))))))))))
(unicode-move-word unicode-backward-word char-before backward-char)
(unicode-move-word unicode-forward-word char-after forward-char)

(defun unicode-backward-kill-word (&optional arg)
  (interactive "p")
  (kill-region (point) (progn (unicode-backward-word arg) (point))))

(defun dwim-isearch-backward (&optional regexp-p no-recursive-edit)
  "If on highlighted symbol, jumps to previous symbol.
Otherwise, performs regular incremental backward search."
  (interactive "P\np")
  (let ((symbol (highlight-symbol-get-symbol)))
    (if (and symbol (member symbol highlight-symbol-list))
        (highlight-symbol-jump -1)
      (isearch-backward regexp-p no-recursive-edit))))

(defun dwim-isearch-forward (&optional regexp-p no-recursive-edit)
  "If on highlighted symbol, jumps to next symbol.
Otherwise, performs regular incremental forward search."
  (interactive "P\np")
  (let ((symbol (highlight-symbol-get-symbol)))
    (if (and symbol (member symbol highlight-symbol-list))
        (highlight-symbol-jump 1)
      (isearch-forward regexp-p no-recursive-edit))))


(defun finish-isearch-ace-jump-mode ()
  (interactive)
  (isearch-done)
  (ace-jump-mode 1))


(defun ideal-frame-width (windows)
  "Given a number of 80 character wide, balanced horizontal windows
   returns the ideal frame width to contain them
   (this seems to be font-size-specific)"
  (assert (> windows 0))
  (+ (* windows 80) (* (- windows 1) 4)))

(defun frame-width-in-windows ()
  (max (round (+ (round (frame-width) 4) 1) 21) 1))

(defun shrink-frame ()
  "Shrink frame to fit one less 80 character wide window"
  (interactive)
  (when window-system
    (let ((windows (frame-width-in-windows)))
      (when (> windows 1)
        (set-frame-width (selected-frame) (ideal-frame-width (- windows 1)))))))

(defun widen-frame ()
  "Enlarge frame to fit one additional 80 character wide window"
  (interactive)
  (when window-system
    (let ((windows (frame-width-in-windows)))
      (set-frame-width (selected-frame) (ideal-frame-width (+ windows 1))))))


(defun start-or-continue-highlighting-line (times)
  "Start highlighting current line, or continue highlighting lines"
  (interactive "p")
  (dotimes (i times)
    (if mark-active
        (progn (beginning-of-line)
               (forward-line 1))
      (beginning-of-line)
      (cua-set-mark)
      (forward-line 1))))

(defun back-to-indentation-or-beginning ()
  (interactive)
  (cond ((bolp) (forward-line -1))
                 ((= (point) (save-excursion (back-to-indentation) (point)))
                            (beginning-of-line))
                 (t (back-to-indentation))))

(defun end-of-line-or-next ()
  (interactive)
  (cond ((eolp) (forward-line 1) (end-of-line))
        (t (end-of-line))))

(defun kill-to-indentation-or-beginning ()
  (let ((indented (- (point)
                     (save-excursion (back-to-indentation)
                                     (point)))))
    (if (= indented 0)
        (kill-line 0)
      (kill-backward-chars indented))))

(defun dwim-kill-line-to-indent ()
  "If not at end of line, kills everything to end of line.
If at beginning and end of line, kills whole line.
Otherwise, kills everything from point to end of identation."
  (interactive)
  (if (not (eolp))
      (kill-line)
    (if (bolp)
        (kill-whole-line)
      (kill-to-indentation-or-beginning))))

(defun dwim-kill-line ()
  "If not at end of line, kills everything to end of line.
If at beginning and end of line, kills whole line.
Otherwise, kills everything from point to beginning of line."
  (interactive)
  (if (not (eolp))
      (kill-line)
    (if (bolp)
        (kill-whole-line)
      (kill-line 0))))

(defun paste-or-paste-pop (arg)
  "Paste, or walk through kill ring"
  (interactive "P")
  (if (not (eq last-command 'yank))
      (cua-paste arg)
    (cua-paste-pop arg)))

(defun duplicate-region ()
  (let* ((end (region-end))
         (text (buffer-substring (region-beginning)
                                 end)))
    (goto-char end)
    (insert text)
    (push-mark end)
    (setq deactivate-mark nil)
    (exchange-point-and-mark)))

(defadvice windmove-left (around windmove activate)
  (condition-case nil
      ad-do-it
    (error nil)))

(defadvice windmove-right (around windmove activate)
  (condition-case nil
      ad-do-it
    (error nil)))

(defadvice windmove-up (around windmove activate)
  (condition-case nil
      ad-do-it
    (error nil)))

(defadvice windmove-down (around windmove activate)
  (condition-case nil
      ad-do-it
    (error nil)))

(defun swap-with (dir)
  (interactive)
  (let ((other-window (windmove-find-other-window dir)))
    (when other-window
      (let* ((this-window  (selected-window))
             (this-buffer  (window-buffer this-window))
             (other-buffer (window-buffer other-window))
             (this-start   (window-start this-window))
             (other-start  (window-start other-window))
             (this-point (window-point this-window))
             (other-point (window-point other-window)))
        (set-window-buffer this-window  other-buffer)
        (set-window-buffer other-window this-buffer)
        (set-window-buffer-start-and-point this-window other-buffer
                                           other-start other-point)
        (set-window-buffer-start-and-point other-window this-buffer
                                           this-start this-point)))))

(defun swap-with-down ()
  (interactive)
  (swap-with 'down))

(defun swap-with-up ()
  (interactive)
  (swap-with 'up))

(defun swap-with-left ()
  (interactive)
  (swap-with 'left))

(defun swap-with-right ()
  (interactive)
  (swap-with 'right))

;; User-defined keybindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; row 0
(global-set-key [f1]                    'goto-line)
(global-set-key [f2]                    'dwim-narrow-to-region)
(global-set-key [f3]                    'centered-cursor-mode)
(global-set-key [f4]                    'highlight-symbol-at-point)
(global-set-key [(control f4)]          'highlight-symbol-query-replace)

(global-set-key [f5]                    (lambda () (interactive)
                                          (condition-case nil
                                              (call-last-kbd-macro)
                                            (error (message
                                                    "C-f5 to define macro")))))
(global-set-key [(control f5)]          'start-or-end-macro)
;; [f6] - undefined
;; [f7] - undefined
;; [f8] - undefined

(global-set-key [f9]                    'query-replace)
(global-set-key [(control f9)]          'query-replace-regexp)
(global-set-key [f10]                   'magit-status)
(global-set-key [f11]                   'undo-tree-visualize)
(global-set-key [f12]                   'compile)

;; row 1
(global-set-key [(control backspace)]   'jl/join-line-backward)
(global-set-key [(control kp-delete)]   'jl/join-line-forward)
(global-set-key [(control delete)]      'jl/join-line-forward)

;; row 2
(global-unset-key [(control q)])
(global-unset-key [(meta q)])
(global-set-key [(control w)]           'unicode-backward-kill-word)

(define-key minibuffer-local-filename-completion-map [(control w)]
  (lambda () (interactive)
    (let ((dir-separator ?/))
      (condition-case nil
          (progn (when (= (char-before) dir-separator) (delete-backward-char 1))
                 (while (/= (char-before) dir-separator)
                   (delete-backward-char 1)))
        (text-read-only nil)))))

(global-unset-key [(meta w)])
(global-set-key [(control r)]           'dwim-isearch-backward)
(global-set-key [(control meta r)]      'isearch-backward-regexp)
(global-unset-key [(meta r)])
(global-unset-key [(control t)])
(global-unset-key [(meta t)])
(global-unset-key [(control y)])
(global-unset-key [(meta y)])
(global-unset-key [(meta u)])
(global-unset-key [(meta i)])
(global-set-key [(control o)]           'find-file)
(global-unset-key [(meta o)])
(global-unset-key [(control p)])
(global-unset-key [(meta p)])
(global-set-key [(control {)]           'shrink-frame)
(global-set-key [(control })]           'widen-frame)

;; row 3
(global-set-key [(control a)]           'start-or-continue-highlighting-line)
(global-unset-key [(meta a)])
(global-set-key [(control s)]           'dwim-isearch-forward)
(global-set-key [(meta s)]              'ace-jump-mode)

(define-key isearch-mode-map [(meta s)] 'finish-isearch-ace-jump-mode)

(global-set-key [(control meta s)]      'isearch-forward-regexp)
(global-unset-key [(control d)])
(global-unset-key [(meta d)])
(global-unset-key [(control f)])
(global-unset-key [(meta f)])
;;(control g) - keyboard-quit
(global-set-key [(meta g)]              'goto-line)
(global-unset-key [(meta h)])
(global-set-key [(control k)]           'dwim-kill-line)
(global-set-key [(control meta k)]      'kill-whole-line)
(global-set-key [(control l)]           'switch-to-buffer)
(global-set-key [(control shift l)]     'redraw-display)
(global-unset-key [(meta l)])
(global-set-key [(control ?\;)]         'comment-dwim)
(global-unset-key [(control ?\')])
(global-unset-key [(meta ?\')])

(define-key cua--cua-keys-keymap [(control return)]
  (lambda () (interactive)
    (if mark-active
        (duplicate-region)
      (end-of-line)
      (newline-and-indent))))


;; row 4
;;(control z) - undo-tree-undo
(global-unset-key [(meta z)])

(global-set-key [(control x) left]      'windmove-left)
(global-set-key [(control x) right]     'windmove-right)
(global-set-key [(control x) up]        'windmove-up)
(global-set-key [(control x) down]      'windmove-down)

(global-set-key [(control x)
                 (shift left)]          'swap-with-left)
(global-set-key [(control x)
                 (shift right)]         'swap-with-right)
(global-set-key [(control x)
                 (shift up)]            'swap-with-up)
(global-set-key [(control x)
                 (shift down)]          'swap-with-down)

(global-unset-key [(meta c)])
(define-key cua--cua-keys-keymap [(control v)]
  'paste-or-paste-pop)
(global-unset-key [(meta v)])
(global-unset-key [(control b)])
(global-unset-key [(meta b)])
(global-set-key [(control n)]           'make-frame-command)
(global-unset-key [(meta n)])

;; separate keys

;;(control space) - cua-set-mark
;;(meta space) - just-one-space
(dolist (keybinding '("C-@" "C-SPC"))
  (eval `(define-key isearch-mode-map (kbd ,keybinding)
           (lambda () (interactive)
             (let ((isearch-beginning isearch-other-end))
               (isearch-done)
               (unless mark-active
                 (setq deactivate-mark nil)
                 (set-mark isearch-beginning)))))))


(global-set-key [home]                  'back-to-indentation-or-beginning)
(global-set-key [end]                   'end-of-line-or-next)
;;(control home) - beginning-of-buffer
;;(control end)  - end-of-buffer
(global-set-key [(control prior)]       'iflipb-previous-buffer)
(global-set-key [(control next)]        'iflipb-next-buffer)

(global-set-key [(up)]                  'vert-previous-line)
(global-set-key [(down)]                'vert-next-line)
(global-set-key [(prior)]               'vert-backward-scroll)
(global-set-key [(next)]                'vert-forward-scroll)
(global-set-key [(wheel-up)]            'vert-backward-scroll)
(global-set-key [mouse-4]               'vert-backward-scroll)
(global-set-key [(wheel-down)]          'vert-forward-scroll)
(global-set-key [mouse-5]               'vert-forward-scroll)
(global-set-key [(control wheel-up)]    'vert-previous-line)
(global-set-key [(control wheel-down)]  'vert-next-line)
(global-set-key [(control mouse-4)]     'vert-previous-line)
(global-set-key [(control mouse-5)]     'vert-next-line)

(global-set-key [(control left)]        'unicode-backward-word)
(global-set-key [(control right)]       'unicode-forward-word)

(global-set-key [(control meta down)]   'swap-with-down)
(global-set-key [(control meta up)]     'swap-with-up)
(global-set-key [(control meta left)]   'swap-with-left)
(global-set-key [(control meta right)]  'swap-with-right)

;; Cut with right button when region active
;; otherwise paste
(global-set-key [mouse-3]               '(lambda (reg) (interactive "P")
                                           (if mark-active (cua-cut-region reg)
                                             (paste-or-paste-pop reg))))

(provide 'keybindings)
