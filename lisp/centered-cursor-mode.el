;;; centered-cursor-mode.el --- cursor stays vertically centered

;; Copyright (C) 2007  André Riemann

;; Author: André Riemann <andre.riemann@web.de>
;; Maintainer: André Riemann <andre.riemann@web.de>
;; Created: 2007-09-14
;; Keywords: convenience

;; URL: http://www.emacswiki.org/cgi-bin/wiki/centered-cursor-mode.el
;; Compatibility: only tested with GNU Emacs 23.0
;; Version: 0.5.2
;; Last-Updated: 2009-08-31

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING. If not, write to the Free
;; Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;; MA 02110-1301, USA.

;;; Commentary:

;; Makes the cursor stay vertically in a defined position (usually
;; centered). The vertical position can be altered, see key definition
;; below.

;; To load put that in .emacs:
;;     (require 'centered-cursor-mode)
;; To activate do:
;;     M-x centered-cursor-mode
;; for buffer local or
;;     M-x global-centered-cursor-mode
;; for global minor mode.
;; Also possible: put that in .emacs
;;     (and
;;      (require 'centered-cursor-mode)
;;      (global-centered-cursor-mode +1))
;; to always have centered-cursor-mode on in all buffers.

;;; TODO:
;; - the code is a mess
;; - ccm-vpos-inverted doesn't work with ccm-vpos == 0, because first
;;   position from top is 0 and from bottom -1
;; - interactive first start isn't animated when calling global-...
;;   because it starts the modes for each buffer and interactive-p fails
;;   for that
;; - more bugs?

;;; Change Log:
;; 2009-08-31  andre-r
;;   * replaced window-body-height with window-text-height
;;     (partially visible lines are not counted in window-text-height)
;;   * bug fixed in ccm-vpos-recenter
;;     (some parentheses where wrong after the last update)
;; 2009-02-23  andre-r
;;   * some simplifications
;; 2009-02-22  andre-r
;;   * some tips from Drew Adams:
;;     - new local variable coding:utf-8
;;     - made recenter-sequence a defvar
;;     - added groups scrolling and convenience
;;     - replaced mouse-4 and mouse-5 with
;;       mouse-wheel-up-event and mouse-wheel-down-event
;;     - added scroll-bar-toolkit-scroll to ccm-ignored-commands
;;     - made ccm-ignored-commands customisable
;;   * removed a bug where it didn't work with more than one window
;;     displaying the same buffer
;;   * added function for page up and down scrolling
;;     (standard ones didn't work well with this mode)
;;   * made the animation delay customisable
;;   * made the initial vertical position customisable
;;   * made the behaviour at the end of the file customisable
;; 2008-02-02  andre-r
;;   * fixed bug that led to wrong-type-argument
;;     when opening a new buffer
;;   * some other minor stuff
;; 2007-09-24  andre-r
;;   * added global minor mode
;; 2007-09-21  andre-r
;;   * not recentering at end of buffer
;;   * defvar animate-first-start-p
;; 2007-09-14  andre-r
;;   * inital release

;; This file is *NOT* part of GNU Emacs.

;;; Code:
 
(defgroup centered-cursor nil
  "Makes the cursor stay vertically in a defined position (usually centered).
Instead the cursor the text moves around the cursor."
  :group 'scrolling
  :group 'convenience
  :link '(emacs-library-link :tag "Source Lisp File" "centered-cursor-mode.el")
  :link '(url-link "http://www.emacswiki.org/cgi-bin/wiki/centered-cursor-mode.el"))

(defcustom ccm-ignored-commands '(mouse-drag-region
                                  mouse-set-point
                                  widget-button-click
                                  scroll-bar-toolkit-scroll
                                  mouse-drag-drag)
  "After these commands recentering is ignored.
This is to prevent unintentional jumping (especially when mouse
clicking). Following commands (except the ignored ones) will
cause an animated recentering to give a feedback and not just
jumping to the center."
  :group 'centered-cursor
  :tag "Ignored commands"
  :type '(repeat (symbol :tag "Command")))

(defcustom ccm-vpos-init '(round (window-text-height) 2)
  "This is the screen line position where the cursor initially stays."
  :group 'centered-cursor
  :tag "Vertical cursor position"
  :type '(choice (const :tag "Center" (round (window-text-height) 2))
                 (const :tag "Golden ratio" (round (* 21 (window-text-height)) 34))
                 (integer :tag "Lines from top" :value 10)))

(defcustom ccm-vpos-inverted 1
  "Inverted vertical cursor position.
Defines if the initial vertical position `ccm-vpos-init' is
measured from the bottom instead from the top."
  :group 'centered-cursor
  :tag "Inverted cursor position"
  :type '(choice (const :tag "Inverted" -1)
                 (const :tag "Not inverted" 1)))

(defcustom ccm-recenter-at-end-of-file nil
  "Recenter at the end of the file.
If non-nil the end of the file is recentered. If nil the end of
the file stays at the end of the window."
  :group 'centered-cursor
  :tag "Recenter at EOF"
  :type '(choice (const :tag "Don't recenter at the end of the file" nil)
                 (const :tag "Recenter at the end of the file" t)))

(defun ccm-position-cursor ()
  "Do the actual recentering at the position `ccm-vpos'."
  (unless (member this-command ccm-ignored-commands)
    (unless (minibufferp (current-buffer))
      (if (equal (current-buffer)
                 (window-buffer (selected-window)))
          (let* ((ccm-vpos (* (eval ccm-vpos-init)
                              (eval ccm-vpos-inverted)))
                 (recenter-at-end-of-file (eval ccm-recenter-at-end-of-file)))

            (let* ((bottom-vpos (if (< ccm-vpos 0)
                                    (- ccm-vpos)
                                  (- (window-text-height) ccm-vpos)))
                   (correction (save-excursion
                                 (if (or (= (point) (point-max))
                                         (progn
                                           (goto-char (point-max))
                                           (zerop (current-column))))
                                     1 0)))
                   ;; lines from point to end of buffer
                   (bottom-lines (+ (count-lines (point) (point-max))
                                    correction)))

              (recenter (if (and (< bottom-lines bottom-vpos)
                                 (not recenter-at-end-of-file))
                            ;; if near the bottom, recenter in the
                            ;; negative screen line that equals the
                            ;; bottom buffer line, i.e. if we are in
                            ;; the second last line (-2) of the
                            ;; buffer, the cursor will be recentered
                            ;; in -2
                            (- bottom-lines)
                          ccm-vpos))))))))

;;(defalias 'ccm 'centered-cursor-mode)
(define-minor-mode centered-cursor-mode
  "Makes the cursor stay vertically in a defined
position (usually centered)."
  :init-value nil
  :lighter " ¢"
  (cond
   (centered-cursor-mode
    (add-hook 'post-command-hook 'ccm-position-cursor t t))
   (t
    (remove-hook 'post-command-hook 'ccm-position-cursor t))))


(define-global-minor-mode global-centered-cursor-mode centered-cursor-mode
  centered-cursor-mode)

(provide 'centered-cursor-mode)

;;; Help:
;; (info "(elisp)Defining Minor Modes")
;; (info "(elisp)Screen Lines")
;; (info "(elisp)Hooks")
;; (info "(elisp)Customization")
;; (find-function 'mwheel-scroll)
 
;; Local Variables:
;; coding: utf-8
;; End:
 
;;; centered-cursor-mode.el ends here
