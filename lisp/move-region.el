(require 'easy-mmode)

;; Copyright (C) 2013, Brian Langenberger, all rights reserved
;; Created: 08 January 2013

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; To install, place:
;;
;; (require 'passwdgen)
;;
;; in your .emacs file.
;;
;; Activate with:
;;
;; M-x move-region-mode
;;
;; or place:
;;
;; (move-region-mode 1)
;;
;; in your .emacs file to activate on startup.
;;
;;
;; When a region is selected such that the beginning and end
;; are at the start of lines, this minor mode allows one
;; to move that region with ctrl-arrow keys.
;; When a region is not selected or selected differently,
;; ctrl-arrow keys work as defined in the global key binding.

(defun move-region/multiple-lines-selected-p ()
  (if (region-active-p)
      (let ((start (region-beginning))
            (end (region-end)))
        (and (> end start)
             (save-excursion
               (goto-char start)
               (bolp))
             (save-excursion
               (goto-char end)
               (bolp))))
    nil))

(defun move-region/move-region (start end n)
  "Move the current region up or down by N lines."
  (interactive "r\np")
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (let ((start (point)))
      (insert line-text)
      (setq deactivate-mark nil)
      (set-mark start))))

(defun move-region/move-region-up (start end n)
  "Move the current line up by N lines."
  (move-region/move-region start end (if (null n) -1 (- n))))

(defun move-region/move-region-down (start end n)
  "Move the current line down by N lines."
  (move-region/move-region start end (if (null n) 1 n)))

(defun move-region/ctrl-left (&optional arg)
  "If mulitple line region is selected, move region left.
Otherwise, perform normal control left action."
  (interactive "P")
  (if (move-region/multiple-lines-selected-p)
      (progn
        (when (> (mark t) (point))
          (exchange-point-and-mark))
        (dotimes (i (if arg arg 1))
          (let ((start (region-beginning))
                (end (region-end)))
            (save-excursion
              (setq end (copy-marker end))
              (goto-char start)
              (while (< (point) end)
                (or (and (bolp) (eolp))
                    (delete-char 1))
                (forward-line 1))
              (move-marker end nil))
            (setq deactivate-mark nil)
            (set-mark start))))
    (call-interactively (global-key-binding [(control left)]))))

(defun move-region/ctrl-right (&optional arg)
  "If mulitple line region is selected, move region right.
Otherwise, perform normal control right action."
  (interactive "P")
  (if (move-region/multiple-lines-selected-p)
      (progn
        (when (> (mark t) (point))
          (exchange-point-and-mark))
        (dotimes (i (if arg arg 1))
          (let ((start (region-beginning))
                (end (region-end)))
            (save-excursion
              (setq end (copy-marker end))
              (goto-char start)
              (while (< (point) end)
                (or (and (bolp) (eolp))
                    (insert " "))
                (forward-line 1))
              (move-marker end nil))
            (setq deactivate-mark nil)
            (set-mark start))))
    (call-interactively (global-key-binding [(control right)]))))

(defun move-region/ctrl-up (&optional arg)
  "If mulitple line region is selected, move region up.
Otherwise, perform normal control up action."
  (interactive "P")
  (if (move-region/multiple-lines-selected-p)
      (move-region/move-region-up (region-beginning)
                                  (region-end)
                                  (if arg arg 1))
    (call-interactively (global-key-binding [(control up)]))))

(defun move-region/ctrl-down (&optional arg)
  "If mulitple line region is selected, move region down.
Otherwise, perform normal control down action."
  (interactive "P")
  (if (move-region/multiple-lines-selected-p)
      (move-region/move-region-down (region-beginning)
                                    (region-end)
                                    (if arg arg 1))
    (call-interactively (global-key-binding [(control down)]))))



(easy-mmode-define-minor-mode
 move-region-mode
 "minor mode for moving regions using arrows when highlighted"
 :init-value nil
 :lighter " M"
 :keymap '(([(control left)]  . move-region/ctrl-left)
           ([(control right)] . move-region/ctrl-right)
           ([(control up)]    . move-region/ctrl-up)
           ([(control down)]  . move-region/ctrl-down)))

(provide 'move-region)
