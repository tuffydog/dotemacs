;;; framedrag.el -- Move frames by clicking anywhere with a mouse.

;; Copyright (C) 2012, Brian Langenberger, all rights reserved
;; Created: 27 March 2012

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; On many X11-based window managers, one can alt-left click anywhere
;; within an X11 window and drag it around the screen.
;; This replicates that functionality for Emacs frames
;; for systems that don't support it natively.
;;
;; To install, place:
;;
;; (require 'framedrag)
;;
;; in one's .emacs file and add the keybinding:
;;
;; (global-set-key [M-down-mouse-1] 'framedrag/mouse-click)
;;
;; Though not as smooth as dragging provided by a window manager
;; I find it fast enough where needed.

(defun framedrag/mouse-click (start-event)
  (interactive "e")
  (framedrag/mouse-track start-event))

(defun framedrag/mouse-track (start-event)
  (interactive "e")
  (let* ((original-position (mouse-pixel-position))
         (frame (car original-position))
         (original-x (cadr original-position))
         (original-y (cddr original-position)))
    (track-mouse
      (while (progn (setq event (read-event))
                    (let* ((new-position (mouse-pixel-position))
                           (delta-x (- (cadr new-position) original-x))
                           (delta-y (- (cddr new-position) original-y)))
                      (framedrag/move-frame frame delta-x delta-y))
                    (or (mouse-movement-p event)
                        (memq (car-safe event)
                              '(switch-frame select-window))))))))

(defsubst framedrag/default-zero (v)
  (if (integerp v) v 0))

(defun framedrag/move-frame (frame delta-x delta-y)
  (let ((maximum-frame-x (- (display-pixel-width)
                            (frame-pixel-width frame)))
        (maximum-frame-y (- (display-pixel-height)
                            (frame-pixel-height frame))))
    (set-frame-position
     frame
     (max (min (+ (framedrag/default-zero
                   (frame-parameter frame 'left)) delta-x)
               maximum-frame-x) 0)
     (max (min (+ (framedrag/default-zero
                   (frame-parameter frame 'top)) delta-y)
               maximum-frame-y) 0))))

(provide 'framedrag)
