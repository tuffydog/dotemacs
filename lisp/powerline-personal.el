;; Personal PowerLine definition and helper functions

(require 'powerline)
(require 'which-func)

(defun pl/buffer-status ()
  (cond ((buffer-modified-p) "*")
        (buffer-read-only "%")
        (t " ")))


;; (defun pl/buffer-name ()
;;   (propertize "%b " 'help-echo (format "%S" buffer-file-coding-system)))


(defun pl/minor-mode-alist-filter (list)
  (if list
      (let ((mode (nth 0 (car list)))
            (value (nth 1 (car list)))
            (next (cdr list)))
        (if (or (eq mode 'centered-cursor-mode)
                (eq mode 'overwrite-mode))
            (cons (list mode value)
                  (pl/minor-mode-alist-filter next))
          (pl/minor-mode-alist-filter next)))
    nil))


(defun pl/vc-status ()
  (let ((name (buffer-file-name)))
    (if name
        (let ((backend (vc-backend name)))
          (if backend
              (format " %s " (vc-default-mode-line-string backend name))
            "   "))
      "   ")))


(defun pl/line-number ()
  (propertize "%5l:%2c "
              'help-echo "goto line"
              'keymap
              (eval-when-compile
                (let ((map (make-sparse-keymap)))
                  (define-key map [mode-line mouse-1]
                    'goto-line)
                  map))
              'mouse-face 'mode-line-highlight))


(defun pl/line-endings ()
  (let* ((eol (coding-system-eol-type buffer-file-coding-system))
         (mnemonic (coding-system-eol-type-mnemonic
                    buffer-file-coding-system)))
    (propertize
     mnemonic
     'help-echo
     (format "End-of-line style: %s\nmouse-1 to cycle"
             (if (eq eol 0) "Unix-style LF"
               (if (eq eol 1) "DOS-style CRLF"
                 (if (eq eol 2) "Mac-style CR"
                   "Undecided"))))
     'keymap
     (eval-when-compile
       (let ((map (make-sparse-keymap)))
         (define-key map [header-line mouse-1] 'mode-line-change-eol)
         map)))))


(defun pl/file-encoding ()
  (let ((coding (coding-system-base buffer-file-coding-system)))
    (propertize
     (if (or (eq coding 'undecided)
             (eq coding 'no-conversion))
         " "
       (format " %S" coding))
     'keymap
     (eval-when-compile
       (let ((map (make-sparse-keymap)))
         (define-key map [header-line mouse-1] 'set-buffer-file-coding-system)
         map)))))


(defconst which-func-current2
  '(:eval (concat (replace-regexp-in-string
                   "%" "%%"
                   (or (gethash (selected-window) which-func-table)
                       which-func-unknown)) " ")))


(defun powerline-personal ()
  "Setup personal PowerLine bar on windowing systems"
  (interactive)
  (setq-default
   mode-line-format
   '("%e" (:eval
           (let* ((active (powerline-selected-window-active))
                  (mode-line (if active 'mode-line 'mode-line-inactive))
                  (face1 (if active 'powerline-active1
                           'powerline-inactive1))
                  (face2 (if active 'powerline-active2
                           'powerline-inactive2))
                  (lhs (list
                        (powerline-raw (pl/buffer-status) face1 'l)
                        (powerline-raw "%b " face1 'l)
                        (powerline-arrow-right face1 face2)
                        (powerline-raw which-func-current2 face2 'l)
                        (powerline-arrow-right face2 nil)
                        (powerline-raw (pl/minor-mode-alist-filter
                                        minor-mode-alist) nil 'l)
                        (powerline-raw "%M" nil 'l)))
                  (rhs (list
                        (powerline-arrow-left nil face1)
                        (powerline-raw (pl/line-number) face1 'r))))
             (concat
              (powerline-render lhs)
              (powerline-fill nil (powerline-width rhs))
              (powerline-render rhs))))))
  (setq-default
   header-line-format
   '("%e" (:eval
           (let* ((active (powerline-selected-window-active))
                  (mode-line (if active 'mode-line 'mode-line-inactive))
                  (face1 (if active 'powerline-active1
                           'powerline-inactive1))
                  (face2 (if active 'powerline-active2
                           'powerline-inactive2))
                  (lhs (list
                        (powerline-raw "  %m " face1 'l)
                        (powerline-arrow-right face1 face2)
                        (powerline-raw (pl/line-endings) face2 'l)
                        (powerline-arrow-right face2 nil)))
                  (rhs (list
                        (powerline-arrow-left nil face2)
                        (powerline-raw (pl/file-encoding) face2 'r)
                        (powerline-arrow-left face2 face1)
                        (powerline-raw (pl/vc-status) face1 'r))))
             (concat (powerline-render lhs)
                     (powerline-fill nil (powerline-width rhs))
                     (powerline-render rhs)))))))


(provide 'powerline-personal)
