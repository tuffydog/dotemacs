;; Update iswitchb for browser-like buffer chooser
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun iswitchb-local-keys ()
  (mapc (lambda (K)
          (let* ((key (car K)) (fun (cdr K)))
            (define-key iswitchb-mode-map (edmacro-parse-keys key) fun)))

        '(("<right>" . ignore)
          ("<left>" . ignore)
          ("<up>" . iswitchb-prev-match)
          ("<down>" . iswitchb-next-match))))
(add-hook 'iswitchb-define-mode-map-hook 'iswitchb-local-keys)

;; Auto-Save when visiting files
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun save-buffer-if-visiting-vc-file (&optional args)
  "Save the current buffer only if it is visiting a file and the
file is under version control and the file is not remote"
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (and filename
             (vc-backend filename)
             (not (file-remote-p filename))
             (buffer-modified-p))
        (save-buffer args))))

(add-hook 'auto-save-hook 'save-buffer-if-visiting-vc-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mode-specific hooks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro programming-mode (mode-hook
                            mode-map
                            pair-keybindings
                            pair-chars
                            smart-tabs-mode)
  `(add-hook ,mode-hook
             '(lambda ()
                ;; make return add a tab in addition to newline
                (define-key ,mode-map [return] 'newline-and-indent)
                (define-key ,mode-map (kbd "RET") 'newline-and-indent)

                ;; perform automatic pairing of parens
                (dolist (C ,pair-keybindings)
                  (define-key ,mode-map C
                    'skeleton-pair-insert-maybe))

                ;; perform automatic unpairing of parens on delete
                (dolist (C (list [backspace] (kbd "DEL")))
                  (define-key ,mode-map C
                    '(lambda (&optional arg)
                       (interactive "p")
                       (if (eq (cdr (assq (char-before) ,pair-chars))
                               (char-after))
                           (and (char-after) (delete-char 1)))
                       (backward-delete-char-untabify arg))))

                (lexical-let ((open-pairs (mapcar 'car ,pair-chars))
                              (paren-chars '(?\( ?\[ ?\{)))
                  ;; add control-end keybinding based on parens
                  (define-key ,mode-map [(control end)]
                    (lambda (arg) (interactive "p")
                      (let ((next-char (char-after)))
                        (if (memq next-char open-pairs)
                            (condition-case nil
                                (if (memq next-char paren-chars)
                                    (forward-list arg)
                                  (forward-sexp arg))
                              (scan-error nil))
                          (while (not (memq (char-after) open-pairs))
                            (forward-char)))))))

                (lexical-let ((close-pairs (mapcar 'cdr ,pair-chars))
                              (paren-chars '(?\) ?\] ?\})))
                  ;; add control-home keybinding based on parens
                  (define-key ,mode-map [(control home)]
                    (lambda (arg) (interactive "p")
                      (let ((prev-char (char-before)))
                        (if (memq prev-char close-pairs)
                            (condition-case nil
                                (if (memq prev-char paren-chars)
                                    (backward-list arg)
                                  (backward-sexp arg))
                              (scan-error nil))
                          (while (not (memq (char-before) close-pairs))
                            (backward-char)))))))

                ;; control-k kills forward if not at end of line
                ;; otherwise, it kills to indentation if ahead
                ;; or kills to beginning of line if at indentation
                (define-key ,mode-map [(control k)] 'dwim-kill-line-to-indent)

                ;; enable move-region mode
                (move-region-mode 1))))

(programming-mode 'c-mode-hook
                  c-mode-map
                  '("\"" "(" "[" "{")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?])
                    (?{  . ?}))
                  nil)

(programming-mode 'c++-mode-hook
                  c++-mode-map
                  '("\"" "(" "[" "{")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?])
                    (?{  . ?}))
                  nil)

(programming-mode 'emacs-lisp-mode-hook
                  emacs-lisp-mode-map
                  '("\"" "(" "[")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?]))
                  nil)

(programming-mode 'java-mode-hook
                  java-mode-map
                  '("\"" "(" "[" "{")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?])
                    (?{  . ?}))
                  nil)

(programming-mode 'latex-mode-hook
                  latex-mode-map
                  '("[" "(" "{" "$")
                  '((?[ . ?])
                    (?( . ?))
                    (?{ . ?})
                    (?$ . ?$))
                  nil)

(programming-mode 'lisp-mode-hook
                  lisp-mode-map
                  '("\"" "(" "[")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?]))
                  nil)

(programming-mode 'php-mode-hook
                  php-mode-map
                  '("\"" "(" "[" "{")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?])
                    (?{  . ?}))
                  nil)

(programming-mode 'python-mode-hook
                  python-mode-map
                  '("\"" "(" "[" "{")
                  '((?\" . ?\")
                    (?\( . ?\))
                    (?[  . ?])
                    (?{  . ?}))
                  nil)

(programming-mode 'sgml-mode-hook
                  sgml-mode-map
                  '("\"" "<")
                  '((?\" . ?\")
                    (?<  . ?>))
                  nil)

(programming-mode 'sql-mode-hook
                  sql-mode-map
                  '("(")
                  '((?\( . ?\)))
                  nil)

(add-hook 'makefile-mode-hook
          '(lambda ()
             (setq indent-tabs-mode t)))

(programming-mode 'nxml-mode-hook
                  nxml-mode-map
                  '("\"" "<")
                  '((?\" . ?\")
                    (?<  . ?>))
                  nil)

(add-hook 'org-mode-hook
          '(lambda ()
             (define-key org-mode-map [(control left)]
               (lambda (arg) (interactive "P")
                 (if mark-active
                     (unicode-backward-word arg)
                   (org-metaleft arg))))
             (define-key org-mode-map [(control right)]
               (lambda (arg) (interactive "P")
                 (if mark-active
                     (unicode-forward-word arg)
                   (org-metaright arg))))
             (define-key org-mode-map [(control up)]
               'org-metaup)
             (define-key org-mode-map [(control down)]
               'org-metadown)
             (define-key org-mode-map [(shift control left)]
               'org-shiftmetaleft)
             (define-key org-mode-map [(shift control right)]
               'org-shiftmetaright)
             (define-key org-mode-map [(shift control up)]
               'org-shiftmetaup)
             (define-key org-mode-map [(shift control down)]
               'org-shiftmetadown)

             (define-key org-mode-map [(meta left)]
               'windmove-left)
             (define-key org-mode-map [(meta right)]
               'windmove-right)
             (define-key org-mode-map [(meta up)]
               'windmove-up)
             (define-key org-mode-map [(meta down)]
               'windmove-down)
             (define-key org-mode-map [(shift meta left)]
               'swap-with-left)
             (define-key org-mode-map [(shift meta right)]
               'swap-with-right)
             (define-key org-mode-map [(shift meta up)]
               'swap-with-up)
             (define-key org-mode-map [(shift meta down)]
               'swap-with-down)))

(add-hook 'eshell-mode-hook
          '(lambda ()
             (setq show-trailing-whitespace nil)
             (define-key eshell-mode-map [home] 'eshell-maybe-bol)
             (define-key eshell-mode-map "C-a" 'eshell-maybe-bol)))

;; punch out of timeclock on exit, if necessary
(add-hook 'kill-emacs-query-functions 'timeclock-query-out)

;; query to exit when pomodoro timer is running
(add-hook 'kill-emacs-query-functions
          (lambda ()
            (if pomodoro-timer
                (yes-or-no-p "Pomodoro timer running, really quit? ")
              t)))

(provide 'hooks)
