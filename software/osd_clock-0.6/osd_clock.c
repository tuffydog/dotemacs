/*
 * osd_clock
 *
 * Copyright (C) 2001-2004, Jon Beckham <leftorium@leftorium.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xosd.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>

static struct option long_options[] = {
  {"font",     1, NULL, 'f'},
  {"color",    1, NULL, 'c'},
  {"format",   1, NULL, 'F'},
  {"delay",    1, NULL, 'd'},
  {"interval", 1, NULL, 'i'},
  {"shadow",   1, NULL, 's'},
  {"top",      0, NULL, 't'},
  {"vcenter",  0, NULL, 'v'},
  {"bottom",   0, NULL, 'b'},
  {"right",    0, NULL, 'r'},
  {"middle",   0, NULL, 'm'},
  {"left",     0, NULL, 'l'},
  {"offset",   1, NULL, 'o'},
  {"hoffset",  1, NULL, 'H'},
  {"help",     0, NULL, 'h'},
  {NULL,       0, NULL, 0}
};

int main (int argc, char *argv[])
{
  char c;

  static const char *format;
  time_t *t = NULL;

  xosd *osd;
  xosd_pos vpos = XOSD_bottom;
  xosd_pos hpos = XOSD_left;

  char *font = "";
  char *color = "red";
  int hoffset = 0,voffset = 0;
  int shadow = 2;
  int delay = 3;
  int interval = 1;

  while ((c = getopt_long(argc ,argv,"f:c:d:F:i:s:o:H:tvbrmlh",
			  long_options, NULL)) != -1)
  {
    switch(c)
    {
      case 'f':
	font = optarg;
	break;
      case 'F':
	format = optarg;
	break;
      case 'c':
	color = optarg;
	break;
      case 'd':
	delay = atoi(optarg);
	break;
      case 'i':
	interval = atoi(optarg);
       break;
      case 's':
	shadow = atoi(optarg);
	break;
      case 'o':
	voffset = atoi(optarg);
	break;
	  case 'H':
	hoffset = atoi(optarg);
	break;
      case 'r':
	hpos = XOSD_right;
	break;
      case 'm':
	hpos = XOSD_center;
	break;
      case 'l':
	hpos = XOSD_left;
	break;
      case 't':
	vpos = XOSD_top;
	break;
      case 'v':
	vpos = XOSD_middle;
	break;
      case 'b':
	vpos = XOSD_bottom;
	break;
      case 'h':
	printf("USAGE: %s [-flag args]\n"
		"\t-f\tfully qualified font.\n"
		"\t-c\tcolor. (default: red)\n"
		"\t-s\tdrop shadow offset. (default: 2)\n"
		"\t-r\tlocate clock at right of screen (default: left)\n"
		"\t-m\tlocate clock in middle of screen (default: left)\n"
		"\t-l\tlocate clock at left of screen (default)\n"
		"\t-t\tlocate clock at top of screen (default: bottom)\n"
		"\t-v\tlocate clock in vertical center of screen (default: bottom)\n"
		"\t-b\tlocate clock at bottom of screen (default)\n"
		"\t-o\tvertical offset in pixels. (default: 0)\n"
		"\t-H\thorizontal offset in pixels. (default: 0)\n"
		"\t-F\tspecify time/date format (in strftime(3) style)\n"
		"\t-d\tdelay (time the clock stays on screen when it's updated)\n"
	        "\t\tin seconds\n"
		"\t-i\tinterval (time between updates) in seconds\n"
		"\t-h\tthis help message\n",
		argv[0]);
	return EXIT_SUCCESS;
	break;
    }
  }

  osd = xosd_create (2);
  if (font != "") xosd_set_font (osd, font);
  xosd_set_colour (osd, color);
  xosd_set_shadow_offset (osd, shadow);
  xosd_set_pos (osd, vpos);
  xosd_set_align (osd, hpos);
  xosd_set_vertical_offset(osd, voffset);
  xosd_set_horizontal_offset(osd, hoffset);


  if (!osd)
  {
    fprintf (stderr, "Error initializing osd\n");
    return EXIT_FAILURE;
  }

  /* If no format is specified, we revert to ctime-ish display */
  if(!format) format = "%a %b %e %H:%M:%S %G";

  while (1)
  {
    time_t curr_time = time(t);
    char output[255];

    strftime(output, 255, format, localtime(&curr_time));

    xosd_display (osd, 1, XOSD_string, output);
    sleep(interval);
  }

  xosd_destroy (osd);

  return EXIT_SUCCESS;
}
