;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Add our library of Lisp stuff to the load path
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq load-path (cons "~/.emacs.d/lisp" load-path))
(setq load-path (cons "~/.emacs.d/lisp/magit" load-path))

(byte-recompile-directory "~/.emacs.d/lisp" 0)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom variables and faces
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(account-info-file "~/.accounts/accounts.json")
 '(auto-save-file-name-transforms (quote ((".*" "~/.emacs.d/backups/" t))))
 '(auto-save-interval 500)
 '(auto-save-timeout 5)
 '(backup-directory-alist (quote ((".*" . "~/.emacs.d/backups/"))))
 '(blink-cursor-mode nil)
 '(c-basic-offset 4)
 '(c-default-style "k&r")
 '(c-syntactic-indentation nil)
 '(ccm-ignored-commands (quote (mouse-drag-region mouse-set-point widget-button-click scroll-bar-toolkit-scroll mouse-drag-drag highlight-symbol-at-point swap-with-down swap-with-up swap-with-left swap-with-right kill-region cua-copy-region redraw-display windmove-left windmove-right windmove-up windmove-down yank yank-pop isearch-printing-char isearch-repeat-forward isearch-repeat-backward ace-jump-mode dwim-isearch-forward dwim-isearch-backward finish-isearch-ace-jump-mode)))
 '(ccm-recenter-at-end-of-file t)
 '(ccm-vpos-init (quote (round (* 21 (window-text-height)) 34)))
 '(column-number-mode t)
 '(compilation-ask-about-save nil)
 '(cua-auto-tabify-rectangles nil)
 '(cua-mode t nil (cua-base))
 '(cursor-in-non-selected-windows nil)
 '(debug-on-error t)
 '(enable-recursive-minibuffers t)
 '(eol-mnemonic-dos "DOS ")
 '(eol-mnemonic-mac "Mac ")
 '(eol-mnemonic-unix " ")
 '(eshell-output-filter-functions (quote (eshell-handle-control-codes eshell-watch-for-password-prompt eshell-postoutput-scroll-to-bottom)))
 '(eshell-scroll-show-maximum-output t)
 '(eshell-scroll-to-bottom-on-output t)
 '(grab-and-drag-enable-inertia nil)
 '(grab-and-drag-gesture-time -1)
 '(hl-line-sticky-flag nil)
 '(hl-paren-colors (quote ("#FF0000" "#F73733" "#EF6E66" "#E7A599")))
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries (quote left))
 '(inhibit-startup-screen t)
 '(isearch-lazy-highlight t)
 '(mouse-yank-at-point t)
 '(pulse-iterations 5)
 '(rst-level-face-base-color "#5f5f5f")
 '(save-place t nil (saveplace))
 '(save-place-file "~/.emacs.d/saved-places")
 '(scroll-bar-mode (quote right))
 '(show-paren-mode t)
 '(smooth-scroll-margin 5)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(uniquify-buffer-name-style (quote post-forward) nil (uniquify))
 '(use-dialog-box nil)
 '(which-func-maxout 0)
 '(xterm-mouse-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(pulse-highlight-start-face ((t (:background "#4f4f4f")))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Get all requried imports
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'ace-jump-mode)           ;; quick movement to area of screen
(require 'alarm)                   ;; alarm alert at specified time
(require 'centered-cursor-mode)    ;; center cursor in window
(require 'cl)                      ;; general-purpose Lisp routines
(require 'framedrag)               ;; alt-left-click to move frames
(require 'framemove)               ;; alt-arrow between frames
(require 'grab-and-drag)           ;; move windows by mouse dragging
(require 'highlight-parentheses)   ;; highlight parens surrounding point
(require 'highlight-symbol)        ;; highlight all instances of symbol
(require 'hooks)                   ;; various hooks for different modes
(require 'iflipb)                  ;; ctrl-pgup/pgdown between buffers
(require 'keybindings)             ;; redefined global keybindings
(require 'lorem-ipsum)             ;; generate dummy text
(require 'magit)                   ;; easier git usage
(require 'malyon)                  ;; play Zork and friends
(require 'move-region)             ;; move highlighted region with ctrl-arrows
(require 'MRU-yank)                ;; treat kill ring as stack
(require 'passwdgen)               ;; generate password to clipboard
(require 'per-window-point)        ;; keep point associated with window
(require 'php-mode)                ;; for editing PHP files
(require 'pomodoro)                ;; time work periods for productivity
(require 'powerline-personal)      ;; better looking status bar
(require 'pulse)                   ;; temporary highlight of pasted region
(require 'python)                  ;; improved Python mode
(require 'repeat-insert)           ;; insert template with template and loop(s)
(require 'rst)                     ;; reStructuredText mode
(require 'saveplace)               ;; save places between sessions
(require 'smart-tab)               ;; conditionally indent or complete text
(require 'smooth-scrolling)        ;; to avoid half-page jumps at window ends
(require 'textgen)                 ;; text generation functions
(require 'timeclock)               ;; log time spent on a project
(require 'undo-tree)               ;; tree-based undo visualizations
(require 'uniquify)                ;; better buffer naming for same filename
(require 'vertical-consistency)    ;; vertical movement works symmetrically
(require 'ws-trim)                 ;; trim whitespace on changed lines


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User interface related items
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Loads a file containing a font size integer
;; (which is technically JSON-encoded), if present.
;; I've used the following sizes at various resolutions:
;; | resolution  | font size |
;; |-------------+-----------|
;; | 2560 x 1440 |        16 |
;; | 1920 x 1200 |        10 |
;; | 1680 x 1050 |        12 |
;; | 1280 x 800  |        14 |
;; but one will need to tweak this to taste.
(if window-system
    (let* ((font-name "Source Code Pro")
           (default-font-size 10)
           (font-size-file (expand-file-name "~/.emacs.d/fontsize.json"))
           (font-size (if (file-readable-p font-size-file)
                          (json-read-file font-size-file)
                        default-font-size))
           (font-name (format "%s-%d" font-name font-size)))
      (set-face-attribute 'default nil :font font-name)
      (add-to-list 'default-frame-alist '(cursor-color . "#FF0000"))
      (add-to-list 'default-frame-alist '(cursor-type . bar))

      ;; Ctrl-left-click to drag window contents, like Adobe Reader
      (global-set-key [(control down-mouse-1)]
                      '(lambda (click) (interactive "e")
                         (centered-cursor-mode 0)
                         (grab-and-drag click)))

      ;; Alt-left-click to drag frame, like X11 window managers
      (global-set-key [M-down-mouse-1] 'framedrag/mouse-click)
      (menu-bar-mode t)

      (setq frame-title-format
            '(buffer-file-name
              "%f" (dired-directory dired-directory "%b"))))

  (menu-bar-mode -1))

;; activate Zenburn theme
(load-theme 'zenburn t)

(let ((zenburn-yellow  "#f0dfaf")
      (zenburn-green+1 "#8fb28f")
      (zenburn-blue    "#8cd0d3")
      (zenburn-bg      "#3f3f3f")
      (zenburn-bg-1    "#2b2b2b")
      (zenburn-bg+1    "#4f4f4f"))

  (set-face-attribute 'mode-line nil
                      :background zenburn-bg
                      :box nil)

  (set-face-attribute 'mode-line-inactive nil
                      :background zenburn-bg
                      :box nil)

  (set-face-attribute 'header-line nil
                      :background zenburn-bg
                      :box nil)

  (set-face-attribute 'powerline-active1 nil
                      :foreground zenburn-yellow
                      :background zenburn-bg-1
                      :box nil)

  (set-face-attribute 'powerline-active2 nil
                      :foreground zenburn-blue
                      :background zenburn-bg+1
                      :box nil)

  (set-face-attribute 'powerline-inactive1 nil
                      :foreground zenburn-yellow
                      :background zenburn-bg-1
                      :weight 'light
                      :slant 'italic
                      :box nil)

  (set-face-attribute 'powerline-inactive2 nil
                      :foreground zenburn-blue
                      :background zenburn-bg+1
                      :weight 'light
                      :slant 'italic
                      :box nil))

;; Command for resizing the current frame
(defun set-width-80 ()
  "Resize frame to 80 characters wide"
  (interactive)
  (set-frame-width (selected-frame) 80))

;; Windmove/Framemmove for keyboard-based frame movement
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings 'meta))
(setq framemove-hook-into-windmove t)

;; Have iswitchb ignore system buffers
(setq iswitchb-buffer-ignore '("\\*"))

;; Activate iswitchb for better buffer switching
(iswitchb-mode 1)

;; Global highlight parentheses
(define-globalized-minor-mode global-highlight-parentheses-mode
  highlight-parentheses-mode
  (lambda () (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)

(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (progn
             ;; Auto indent on paste
             (and (not current-prefix-arg)
                  (member major-mode '(emacs-lisp-mode
                                       lisp-mode
                                       scheme-mode
                                       ;; c-mode
                                       ;; c++-mode
                                       ;; objc-mode
                                       ))
                  (let ((mark-even-if-inactive transient-mark-mode))
                    (indent-region (region-beginning) (region-end) nil)))

             ;; Pulse highlighted region on paste
             (pulse-momentary-highlight-region (region-beginning)
                                               (region-end))))))

;; Create backup directory if it doesn't exist
(unless (file-directory-p "~/.emacs.d/backups")
  (make-directory "~/.emacs.d/backups"))

;; Minibuffer depth indicator
(minibuffer-depth-indicate-mode 99)

;; Winner mode offers easy window restoring
(when (fboundp 'winner-mode) (winner-mode 1))

;; Enable whitespace trim in most buffers
(global-ws-trim-mode 1)

;; Enable smart-tab everywhere
(global-smart-tab-mode 1)

;; undo-tree everywhere
(global-undo-tree-mode 1)

;; enable skeleton-pair-insert-maybe
(setq skeleton-pair t)

;; *.org files always open in org-mode
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

;; start nice PowerLine bar
(powerline-personal)

;; Activate MRU-yank
(setq MRU-yank-mode t)

;; Activate per-window-point
(pwp-mode 1)

;; Activate which-function mode
(which-function-mode 1)

(defun test ()
  (interactive)
  (message "%S" (coding-system-base buffer-file-coding-system)))

;; goes to the beginning of line, maybe, in eshell
(defun eshell-maybe-bol ()
  (interactive)
  (let ((p (point)))
    (eshell-bol)
    (if (= p (point))
        (move-beginning-of-line))))

;; allows the use of upcase-region and downcase-region
;; without a warning from Emacs
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
